package sabih.pcr;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class PasswordReset extends AppCompatActivity {

    EditText email, NewPassword;
    Button Reset;
    ProgressDialog dialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);

        email = (EditText) findViewById(R.id.passEmail);
        NewPassword= (EditText) findViewById(R.id.newPass);
        Reset= (Button) findViewById(R.id.btn_reset);

        Reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(PasswordReset.this, "", "Validating user...", true);
                new Thread(new Runnable() {
                    public void run() {
                        login();
                    }
                }).start();
            }
        });
    }

    void login(){
        try{

            final String staffEmail =  email.getText().toString().trim();
            final String staffPass = NewPassword.getText().toString().trim();

            HttpClient httpclient=new DefaultHttpClient();
            HttpPost httppost= new HttpPost("http://companyphone.deanstechsolutions.com.au/pcr/androidApp/PasswordReset.php"); // make sure the url is correct.
            //add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair("staffEmail", staffEmail));
            nameValuePairs.add(new BasicNameValuePair("staffPass", staffPass));

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            final String response = httpclient.execute(httppost, responseHandler);
            System.out.println("Response : " + response);
            runOnUiThread(new Runnable() {
                public void run() {
                    dialog.dismiss();
                }
            });

            if(response.equalsIgnoreCase("success")){
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(PasswordReset.this,"Success", Toast.LENGTH_SHORT).show();
                    }
                });

                startActivity(new Intent(PasswordReset.this, MainActivity.class));
            }else{
                showAlert();
            }

        }catch(Exception e){
            dialog.dismiss();
            System.out.println("Exception : " + e.getMessage());
        }
    }
    public void showAlert(){
        PasswordReset.this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(PasswordReset.this);
                builder.setMessage("User not Found. Please enter valid email address and password")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}