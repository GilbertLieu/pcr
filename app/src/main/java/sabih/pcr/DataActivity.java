package sabih.pcr;

import android.app.Activity;
import android.app.AlertDialog;
import android.net.TrafficStats;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataActivity extends Activity {

    private Handler mHandler = new Handler();
    private static final String TAG = "DataActivity.java";
    private long mStartRX = 0;
    private long mStartTX = 0;
    TextView RX = null;
    TextView TX = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        long mStartRX = TrafficStats.getTotalRxBytes();
        long mStartTX = TrafficStats.getTotalTxBytes();
        RX = (TextView) findViewById(R.id.RX);
        TX = (TextView) findViewById(R.id.TX);


        if (mStartRX == TrafficStats.UNSUPPORTED || mStartTX == TrafficStats.UNSUPPORTED) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Uh Oh!");
            alert.setMessage("Your device does not support traffic stat monitoring.");
            alert.show();
        } else {
            mHandler.postDelayed(mRunnable, 1000);

        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        try {

             String _sData  = TX.getText().toString();
             String _rData = RX.getText().toString();
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
            nameValuePairList.add(new BasicNameValuePair("sendData", _sData ));
            nameValuePairList.add(new BasicNameValuePair("receiveData", _rData));

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://companyphone.deanstechsolutions.com.au/pcr/androidApp/Data.php");
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairList));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();

            if (httpEntity != null) {

                String responseStr = EntityUtils.toString(httpEntity).trim();
                Log.v(TAG, "Response: " +  responseStr);
                Toast.makeText(getApplicationContext(), "Data Successfully Sent", Toast.LENGTH_SHORT).show();
            }

        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Data Sending Failed", Toast.LENGTH_SHORT).show();
        }

    }

    private final Runnable mRunnable = new Runnable() {
        public void run() {

            // Bytes converted to Megabytes
       long rxBytes = (TrafficStats.getTotalRxBytes() - mStartRX) / (1024 * 1024);
            RX.setText(rxBytes + "MB");

        long txBytes = (TrafficStats.getTotalTxBytes() - mStartTX) / (1024 * 1024);
            TX.setText(txBytes + "MB");

            mHandler.postDelayed(mRunnable, 1000);

        }
    };

}