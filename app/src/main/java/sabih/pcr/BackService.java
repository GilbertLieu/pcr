package sabih.pcr;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class BackService extends Service {

    private static final String TAG = "BackService.java";

    private boolean isRunning  = false;

    @Override
    public void onCreate() {
        Log.i(TAG, "Service onCreate");
        isRunning = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(TAG, "Service onStartCommand");

        //Creating new thread service for send call logs to webserver
        new Thread(new Runnable() {
            @Override
            public void run() {

                //This logic for the service will send the data will be set here in milliseconds
                //In this example we are looping and waits 12 43200000 hours to run each loop.
                for (int i = 0; i < 5; i++) {
                    try {
                        Thread.sleep(43200000 );
                    } catch (Exception e) {

                    }
                    if(isRunning){
                        Log.i(TAG, "Service running");

                        Intent intent1 = new Intent(BackService.this, WebCalls.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent1);

                    }
                }

                //Stop service once it finishes its task
                stopSelf();

            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < 5; i++) {
                    try {
                        Thread.sleep(43200100 );
                    } catch (Exception e) {

                    }

                    if(isRunning){
                        Log.i(TAG, "Service running");

                        Intent intent2 = new Intent(BackService.this, WebSMS.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent2);

                    }
                }

                //Stop service once it finishes its task
                stopSelf();

            }
        }).start();

        return Service.START_STICKY;

    }


    @Override
    public IBinder onBind(Intent arg0) {
        Log.i(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onDestroy() {

        isRunning = false;

        Log.i(TAG, "Service onDestroy");
    }
}